# coding=utf-8
""" Store configurations for Flask in 3 main modes:
  Development & Testing & Production

"""
import logging
import os

__author__ = 'anh.dv'
_logger = logging.getLogger(__name__)

ROOT_DIR = os.path.abspath(os.path.join(
    os.path.dirname(__file__),
))
# The environment to run this config. This value will affect to the
# configuration loading
#
# it can be: dev, test, stag, prod
FLASK_ENV = os.environ.get('FLASK_ENV', '').upper()

DEBUG = True
TESTING = False

LOGGING_CONFIG_FILE = os.path.join(ROOT_DIR, 'etc', 'logging.ini')
FLASK_APP_SECRET_KEY = '4fd8797370363dca2bd870801148d1a6dbca67fbbcd66ed0'
SQLALCHEMY_TRACK_MODIFICATIONS = True
SQLALCHEMY_COMMIT_ON_TEARDOWN = True
CELERY_BROKER_URL = 'redis://localhost:6379'
CELERY_RESULT_BACKEND = 'redis://localhost:6379'
SQLALCHEMY_DATABASE_URI = 'mysql+pymysql://root:river@database/silent'

def _env(name, default):
    """ Get configuration from environment in priorities:
      1. the env var with prefix of $ENV_MODE
      2. the env var with the same name (in upper case)
      3. the default value

    :param str name: configuration name
    :param default: default value
    """

    def _bool(val):
        if not val:
            return False
        return val not in ('0', 'false', 'no')

    # make sure configuration name is upper case
    name = name.upper()

    # try to get value from env variables
    val = default
    for env_var in ('%s_%s' % (FLASK_ENV, name), name):
        try:
            val = os.environ[env_var]
            break
        except KeyError:
            pass
    else:
        env_var = None

    # convert to the right types
    if isinstance(default, bool):
        val = _bool(val)
    return env_var, val


_IGNORED_CONFIG = (
    'ROOT_DIR',
    'STATIC_DIR',
    'ENV_MODE',
)

# rewrite all configuration with environment variables
_vars = list(locals().keys())
for name in _vars:
    if name in _IGNORED_CONFIG:
        continue
    if not name.startswith('_') and name.isupper():
        env_var, val = _env(name, locals()[name])
        locals()[name] = val
