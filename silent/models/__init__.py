# coding=utf-8
import json
import logging
import os

import flask_migrate
import flask_sqlalchemy as _fs
from amqp import NotFound
from sqlalchemy import create_engine, Column

from silent import utils

__author__ = 'anh.dv'
_logger = logging.getLogger(__name__)

db = _fs.SQLAlchemy()
migrate = flask_migrate.Migrate(db=db)


def get_class_by_tablename(table_name):
    for table in db.Model._decl_class_registry.values():
        if hasattr(table, '__tablename__') and table.__tablename__ == table_name:
            return table
    return None


def get_all_table():
    ret = []
    for table in db.Model._decl_class_registry.values():
        if hasattr(table, '__tablename__'):
            ret.append(table)
    return ret


def init_app(app):
    """
    :param flask.Flask app: the app
    :return:
    """
    db.app = app
    db.init_app(app)
    migrate.init_app(app)

    print('Start app with database: %s' %
          app.config['SQLALCHEMY_DATABASE_URI'])


class JSONParsedProperty(object):
    """ Biểu diễn 1 thuộc tính dựa trên việc parse 1 thuộc tính khác của object
    theo JSON
    """

    def __init__(self, raw_property_name):
        """
        :param str raw_property_name: tên của thuộc tính chứa thông tin raw
        """
        self.raw_property_name = raw_property_name
        self.parsed_property_name = '_parsed_%s_' % raw_property_name

    def __get__(self, instance, owner):
        # Trả về giá trị của property nếu gọi = class
        if instance is None:
            return self

        # Nếu giá trị raw bị thay đổi sau khi gọi giá trị JSON, giá trị JSON
        # sẽ không được update lại theo giá trị mới
        try:
            return instance.__getattribute__(self.parsed_property_name)
        except AttributeError:  # this property has not been parsed
            # parse property's value
            raw_val = instance.__getattribute__(self.raw_property_name)
            if raw_val:
                parsed_value = utils.json_decode(raw_val)
            else:
                parsed_value = None

            # set the parsed value, mark property parsed
            setattr(instance, self.parsed_property_name, parsed_value)
            return parsed_value

    def __set__(self, instance, value):
        setattr(instance, self.parsed_property_name, value)
        setattr(instance, self.raw_property_name, utils.json_encode(value))


from .user import User