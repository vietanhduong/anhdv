import logging

from silent.exts import sqlalchemy_utils as _su
from . import db
from silent.utils import Constants

__author__ = 'anh.dv'
_logger = logging.getLogger(__name__)


class User(db.Model, _su.TimestampMixin):
    __tablename__ = 'user'

    email = db.Column(db.String(100), primary_key=True)
    provider = db.Column(db.String(100), primary_key=True)
    password = db.Column(db.String(100))
    first_name = db.Column(db.String(100))
    last_name = db.Column(db.String(100))
    dob = db.Column(db.Date())
    address = db.Column(db.Text())
    is_banned = db.Column(db.Integer(), nullable=False, default=Constants.DE_ACTIVE)
    ban_time = db.Column(db.DateTime())
    is_active = db.Column(db.Integer(), nullable=False, default=Constants.IS_ACTIVE)
