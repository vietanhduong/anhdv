# coding=utf-8
import enum
import logging
import uuid as _uuid

import flask_restplus as fr
from flask import Blueprint
from flask_restplus import Api, fields

__author__ = 'anh.dv'
_logger = logging.getLogger(__name__)


class BaseApi:
    """
        Base API
    """

    def return_general(self, code, msg=None, data=None, http_code=None):
        return {
                   'code': code,
                   'message': msg or 'OK',
                   'data': data
               }, http_code or code


authorizations = {
    'apikey': {
        'type': 'apiKey',
        'in': 'header',
        'name': 'Authorization'
    }
}

api_blueprint = Blueprint('api', __name__, url_prefix='/api')
api = Api(
    api_blueprint,
    version='1.0',
    title='Silent service API',
    description='Silent service API',
    authorizations=authorizations,
    security='apikey'
)
v1 = api.namespace('v1', description='API for Version 1')
user = api.namespace('v1/user', description='API for user')


def wrap_data(namespace):
    uuid = _uuid.uuid1()

    return api.model(
        'Data-%s' % uuid, {
            'code': fields.Integer(),
            'data': fields.Nested(namespace),
            'message': fields.String()
        }
    )


class EnumValue(fr.fields.String):
    def format(self, enum_type):
        if isinstance(enum_type, enum.Enum):
            return enum_type.value

        return enum_type
