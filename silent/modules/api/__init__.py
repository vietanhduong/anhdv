# coding=utf-8
import logging

from . import base, user

__author__ = 'anh.dv'
_logger = logging.getLogger(__name__)


def init_app(app):
    app.register_blueprint(base.api_blueprint)
