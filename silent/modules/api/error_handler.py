# coding=utf-8
import logging
import os

from werkzeug.exceptions import RequestEntityTooLarge

from silent import models as m
from .base import api

__author__ = 'anh.dv'
_logger = logging.getLogger(__name__)


class NotFound(Exception):
    pass


class BadRequest(Exception):
    pass


class UnAuthorized(Exception):
    pass


class Conflict(Exception):
    pass


class AccessDenied(Exception):
    pass


def on_error(code, message):
    m.db.session.rollback()
    return {
               'code': code,
               'message': message
           }, code


@api.errorhandler(NotFound)
def not_found(e):
    _logger.exception(e)
    return on_error(400, str(e) or 'Resource không tồn tại')


@api.errorhandler(AccessDenied)
def access_denied(e):
    _logger.exception(e)
    return on_error(403, str(e) or 'Bạn không có quyền thực hiện thao tác này')


@api.errorhandler(Conflict)
def conflict(e):
    _logger.exception(e)
    return on_error(409, str(e) or 'Conflict error')


@api.errorhandler(UnAuthorized)
def un_authorized(e):
    _logger.exception(e)
    return on_error(401, str(e) or 'Bạn chưa đăng nhập hệ thống')


@api.errorhandler(BadRequest)
def bad_request(e):
    _logger.exception(e)
    return on_error(400, str(e) or 'Dữ liệu không hợp lệ')

@api.errorhandler
def on_unknown_exception(e):
    _logger.exception(e)
    return on_error(500, str(e))


class HandleSpecialError():
    def __init__(self, registers):
        """

        :param Object registers:
        """
        self._registers = registers
        for cls in registers:
            api.errorhandler(cls)(self.error_handler)

    def error_handler(self, e):
        _logger.exception(e)
        cls = e.__class__

        handler = self._registers[cls]
        if isinstance(handler, (tuple, list)):
            code, msg = self._registers[cls]
            return on_error(code, msg or str(e))

        if callable(handler):
            rv = handler(e)
            if isinstance(rv, (list, tuple)):
                code, msg = rv
            else:
                code, msg = 400, rv
            return on_error(code, msg or str(e))

        assert False, 'Invalid handler: %s' % handler


__register__ = {
}

HandleSpecialError(__register__)
