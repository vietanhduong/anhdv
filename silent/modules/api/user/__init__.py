import logging
import flask
from flask_restplus import Resource

from ..base import user, BaseApi
from ..error_handler import NotFound, BadRequest

__author__ = 'anh.dv'
_logger = logging.getLogger(__name__)


@user.route('/')
class UserController(BaseApi, Resource):

    def get(self):
        return self.return_general(code=200, data={
            'message': 'ok'
        })
