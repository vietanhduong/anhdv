# coding=utf-8
import logging

from flask import Blueprint

__author__ = 'anh.dv'
_logger = logging.getLogger(__name__)

web = Blueprint('web_v1', __name__, url_prefix='/web')


def init_app(app):
    app.register_blueprint(web)
