# coding=utf-8
import logging
from . import api
from . import web

__author__ = 'Binh.Tran'
_logger = logging.getLogger(__name__)


def init_app(app):
    api.init_app(app)
    web.init_app(app)
