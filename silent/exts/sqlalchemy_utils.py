# coding=utf-8
import logging
import datetime

import dateutil.tz
import sqlalchemy as _sa
from sqlalchemy import func
from sqlalchemy.ext.declarative import declared_attr

__author__ = 'anh.dv'
_logger = logging.getLogger(__name__)

_local_timezone = dateutil.tz.tzlocal()


def _now_in_local_timezone():
    return datetime.datetime.now(tz=_local_timezone)


class DateTimeWithTimezone(_sa.TypeDecorator):
    impl = _sa.TIMESTAMP

    def process_bind_param(self, value, engine):
        return value

    def process_result_value(self, value, engine):
        _logger.info('----------%s----------', value)
        return value.astimezone()


class TimestampMixin(object):

    @declared_attr
    def created(self):
        return _sa.Column(_sa.TIMESTAMP,
                          server_default=func.now(), nullable=False)

    @declared_attr
    def updated(self):
        return _sa.Column(_sa.TIMESTAMP,
                          server_default=func.now(), nullable=False, onupdate=func.now())
