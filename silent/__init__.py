# coding=utf-8
import logging
from . import _app
from .exts import celery as _celery

__author__ = 'anh.dv'
_logger = logging.getLogger(__name__)

app = _app.create_app()
celery = _celery.make_celery(app)

_app.init_app(app)
