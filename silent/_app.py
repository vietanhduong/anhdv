# coding=utf-8
import logging

import flask
from raven.contrib.flask import Sentry
from silent.modules.api.error_handler import NotFound, UnAuthorized, BadRequest

__author__ = 'anh.dv'
_logger = logging.getLogger(__name__)


def init_app(app, **kwargs):
    """ initialize application
    :param flask.Flask app:
    """
    from . import models
    from . import modules

    models.init_app(app)
    modules.init_app(app)


def init_workers():
    pass


def _after_request(response):
    request = flask.request
    origin = request.headers.get('Origin')

    response.headers['Access-Control-Allow-Origin'] = origin
    response.headers['Access-Control-Allow-Credentials'] = 'true'
    response.headers['Access-Control-Allow-Methods'] = 'GET, POST, PUT, DELETE, OPTIONS'
    response.headers[
        'Access-Control-Allow-Headers'] = 'Origin, X-Requested-With, Content-Type, Accept, Authorization, Content-Disposition'

    return response


def create_app(env=None):
    """ Create Flask application based on env_name
    :rtype: flask.Flask
    """
    import config
    import logging.config
    import os

    env = env or config.FLASK_ENV

    def load_app_config(app):
        """Load app's configurations

        :param flask.Flask app: the app
        """
        instance_config_file = 'config_%s.py' % config.FLASK_ENV.lower()
        app.config.from_object(config)
        app.config.from_pyfile('config.py', silent=True)
        app.config.from_pyfile(instance_config_file, silent=True)

    app = flask.Flask(
        __name__,
        instance_relative_config=True,
        instance_path=os.path.join(config.ROOT_DIR, 'instance'),
    )
    # app.json_encoder = utils.JSONEncoder
    load_app_config(app)
    # setup logging
    logging.config.fileConfig(app.config['LOGGING_CONFIG_FILE'],
                              disable_existing_loggers=False)

    app.secret_key = config.FLASK_APP_SECRET_KEY
    app.after_request(_after_request)

    dns = 'https://c9474fef6a8a4fe7975558b952590a3f@sentry.io/1300927' if os.environ.get(
        'SEND_REPORT') == 'true' else None
    _logger.info(dns)

    app.config['SENTRY_CONFIG'] = {
        'ignore_exceptions': [NotFound, UnAuthorized, BadRequest],
    }
    sentry = Sentry(app, dsn=dns, level=logging.ERROR)

    return app
