# coding=utf-8
import calendar
import datetime
import decimal
import enum
import json as _json
import logging
import re
import time
from time import time
from unittest import mock

import flask.json

__author__ = 'anh.dv'
_logger = logging.getLogger(__name__)


class Constants:
    # active constants
    IS_ACTIVE = 1
    DE_ACTIVE = 0

    DATE = "%Y-%m-%d"
    DATETIME = "%Y-%m-%d %H:%M:%S"


class StringFactory:

    @staticmethod
    def is_none_or_empty(src):
        if src is None or len(str.strip(str(src))) == 0:
            return True
        return False

    @staticmethod
    def convert_string(src, default=None):
        if StringFactory.is_none_or_empty(src):
            return default
        else:
            return str.strip(src)

    @staticmethod
    def like_builder(src):
        if StringFactory.is_none_or_empty(src):
            return None
        src = StringFactory.convert_string(src)
        return '%' + src + '%'


class JSONEncoder(flask.json.JSONEncoder):
    """Customized flask JSON Encoder"""

    def default(self, o):
        from silent import models as m
        if hasattr(o, '__json__'):
            return o.__json__()
        if isinstance(o, decimal.Decimal):
            if o == o.to_integral_value():
                return int(o)
            else:
                return float(o)
        if isinstance(o, (datetime.datetime, datetime.date)):
            return o.isoformat(sep=' ')
        if isinstance(o, enum.Enum):
            return o.value
        if isinstance(o, tuple):
            return list(o)
        if isinstance(o, m.db.Model):
            return o.to_dict
        if isinstance(o, mock.MagicMock):
            return repr(o)
        return super().default(o)


_default_json_encoder = JSONEncoder()
json_encode = _default_json_encoder.encode


def json_decode(s):
    """ Decode a json string
    :param str s: the JSON encoded string
    :rtype: mixed
    """
    return _json.loads(s)


def current_milli_time():
    return int(round(time.time() * 1000))


def id_list_from_entity_list(entities):
    """ Get id list from entity list
    :param entities:
    :return: array of id
    """
    results = []
    for entity in entities:
        results.append(entity.id)
    return results


def entity_list_to_id_map(entities):
    """ Convert entity list to map (id => entity)
    :param entities:
    :return: dict
    """
    results = {}
    for entity in entities:
        results[entity.id] = entity
    return results


def array_to_map(array):
    return {k: v for k, v in array}


def recurse_into(obj, baseaction, basetype=str):
    if isinstance(obj, basetype):
        return baseaction(obj)
    elif isinstance(obj, list):
        return [recurse_into(o, baseaction, basetype) for o in obj]
    elif isinstance(obj, tuple):
        return tuple(recurse_into(o, baseaction, basetype) for o in obj)
    elif isinstance(obj, dict):
        return dict((k, recurse_into(v, baseaction, basetype)) for (k, v) in obj.items())
    else:
        return obj


def generate_recurse(baseaction, basetype=str):
    def f(obj):
        return recurse_into(obj, baseaction, basetype)

    return f


trim = generate_recurse(StringFactory.convert_string)


def safe_cast(val, to_type, default=None):
    try:
        return to_type(val)
    except (ValueError, TypeError):
        return default


def cast_date(val, default=None):
    try:
        if StringFactory.is_none_or_empty(val):
            return default
        return datetime.datetime.strptime(val, Constants.DATE)
    except (ValueError, TypeError):
        return default


def cast_datetime(val, default=None):
    try:
        if StringFactory.is_none_or_empty(val):
            return default
        return datetime.datetime.strptime(val, Constants.DATETIME)
    except (ValueError, TypeError):
        ret = cast_date(val, default)
        if ret:
            return datetime.datetime.combine(ret, datetime.datetime.min.time())
        return default


def get_first_or_default(l, default=None):
    return l[0] if l is not None and len(l) != 0 else default


def unique_id(prefix='HC', campaign_id=0):
    code = '%s-%06d' % (prefix, campaign_id)
    return code


def extract_source_id(source_id):
    ret = {
        'prefix': None,
        'data_id': None
    }
    if StringFactory.is_none_or_empty(source_id):
        return ret

    source_id = re.sub(r'\s+', '', source_id)
    source_id = source_id.lower()
    if len(source_id.split(':')) != 2:
        return ret
    prefix, data_id = source_id.split(':')

    facebook_patterns = ['fb']
    # convert prefix
    if prefix in facebook_patterns:
        prefix = 'facebook'

    return {
        'prefix': prefix,
        'data_id': data_id
    }


def copy_model(src, des):
    primary_keys = [x.name for x in src.__table__.primary_key]
    for c in src.__table__.columns:
        attr = c.name
        if attr in primary_keys:
            continue
        setattr(des, attr, getattr(src, attr, None))


def format_phone(phone):
    if not phone:
        return []
    phones = [phone]
    if str.startswith(phone, '0'):
        phone = phone[1:]
        phones.append(phone)
        phones.append('84' + phone)
        phones.append('+84' + phone)
    elif str.startswith(phone, '84'):
        phone = phone[2:]
        phones.append(phone)
        phones.append('0' + phone)
        phones.append('+84' + phone)
    elif str.startswith(phone, '+84'):
        phone = phone[3:]
        phones.append(phone)
        phones.append('0' + phone)
        phones.append('84' + phone)
    else:
        phones.append('0' + phone)
        phones.append('84' + phone)
        phones.append('+84' + phone)
    return phones


def format_phone_list(phones):
    if phones is None or len(phones) == 0:
        return []
    new_phones = []
    for phone in phones:
        new_phones.extend(format_phone(phone))
    return new_phones


class Asterisk:

    @staticmethod
    def extract_ext(distchannel):
        if StringFactory.is_none_or_empty(distchannel):
            return None
        tmp = distchannel.split('-')
        if len(tmp) != 2:
            return None
        raw_ext = tmp[0]
        tmp = raw_ext.split('/')
        if len(tmp) != 2:
            return None
        ext = tmp[1]
        return safe_cast(ext, int)


def row2dict(row):
    d = {}
    for column in row.__table__.columns:
        d[column.name] = str(getattr(row, column.name))

    return d


def second_to_hour(seconds=0):
    m, s = divmod(seconds, 60)
    h, m = divmod(m, 60)
    return "%d:%02d:%02d" % (h, m, s)


def normal_name(name):
    ret = {
        'first_name': None,
        'full_name': None
    }

    if StringFactory.is_none_or_empty(name):
        return ret

    name = StringFactory.convert_string(name)
    arr = str(name).split()

    full = ''
    for x in arr:
        full += x.capitalize() + ' '

    ret['first_name'] = arr[-1].capitalize()
    ret['full_name'] = full.strip()
    return ret


def extract_email(email):
    ret = {
        'name': None,
        'domain': None
    }
    if StringFactory.is_none_or_empty(email):
        return ret

    email = StringFactory.convert_string(email)
    arr = email.split('@')
    if len(arr) != 2:
        return ret
    ret['name'] = arr[0]
    ret['domain'] = arr[1]
    return ret


def add_months(date, months):
    day = date.day
    new_month = (date.month + months) % 12
    if new_month == 0:
        new_month = 12
    new_year = date.year + (date.month + months) / 12
    day = min(day, calendar.monthrange(new_year, new_month)[1])
    return datetime.date(new_year, new_month, day)


def extract_ext(interface):
    # interface format : Local/2003@from-queue/n"
    if StringFactory.is_none_or_empty(interface):
        return None
    split = interface.split('@')
    if len(split) != 2:
        return None
    # split: Local/2003
    split = split[0].split('/')
    if len(split) != 2:
        return None
    # split: 2003
    return split[-1]
