#!/bin/bash

printenv | sed 's/^\(.*\)$/export \1/g' > /app/var/set_env.sh
chmod +x /app/var/set_env.sh

.venv/bin/flask db upgrade heads

cron
supervisord
