# coding=utf-8
import logging
from silent import app

__author__ = 'anh.dv'
_logger = logging.getLogger(__name__)


if __name__ == '__main__':
    app.run()
