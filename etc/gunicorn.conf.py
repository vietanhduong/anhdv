# coding=utf-8
import logging
import multiprocessing
import os

_author_ = 'anh.dv'
logger = logging.getLogger(__name__)

_ROOT = os.path.abspath(os.path.join(
    os.path.dirname(__file__),
    '..',
))

_VAR = os.path.join(_ROOT, 'var')
_ETC = os.path.join(_ROOT, 'etc')

if os.environ.get('ENV_MODE') == 'dev':
    reload = True
else:
    workers = multiprocessing.cpu_count() * 2 + 1

bind = '0.0.0.0:8000'

timeout = 180  # 3 minutes
keepalive = 24 * 3600  # 1 day
logconfig = os.path.join(_ETC, 'logging.ini')