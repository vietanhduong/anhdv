# Use an official Python runtime as a parent image
FROM tdbinh93/python3.6
ARG DEBIAN_FRONTEND=noninteractive

WORKDIR /app
ADD ./requirements.txt /app/requirements.txt

# Setup flask
RUN python3.6 -m venv .venv
RUN . .venv/bin/activate && pip install -U pip && pip install -r requirements.txt

COPY . /app

# Setup cron
ADD ./bash/crontab /etc/cron.d/scan
RUN chmod 0644 /etc/cron.d/scan

ENV TZ=Asia/Ho_Chi_Minh FLASK_APP=main.py LC_ALL=C.UTF-8 LANG=C.UTF-8 FLASK_DEBUG=1
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone
RUN apt-get update && apt-get install tzdata -y

RUN chmod +x /app/bash/run.sh

#RUN printenv | sed 's/^\(.*\)$/export \1/g' > /app/var/set_env.sh
#RUN chmod +x /app/var/set_env.sh
